function goToTop(){
    window.scrollTo(0, 0);
}
$(window).scroll(function(){
    var sticky = $('.scroll-top'),
        scroll = $(window).scrollTop();

    if (scroll >= 100) sticky.addClass('scroll-to-top');
    else sticky.removeClass('scroll-to-top');
});
$(window).scroll(function(){
    var sticky = $('.menu-main'),
        scroll = $(window).scrollTop();

    if (scroll >= 100) sticky.addClass('menu-scroll');
    else sticky.removeClass('menu-scroll');
});
// menu click
$(".icon-menu").click(function(){
    $('.menu-responsive').addClass('show-menu-right')
});
$(".icon-close-menu").click(function(){
    $('.menu-responsive').removeClass('show-menu-right')
});

$(window).scroll(function(){
    var sticky = $('.menu-tab'),
        scroll = $(window).scrollTop();

    if (scroll >= 200) sticky.addClass('tab-fixed');
    else sticky.removeClass('tab-fixed');
});


// saerch click
$(".icon-search-click .ti-search").click(function(){
    $('.search-box-update').addClass('show-search-udpate')
});
$(".icon-search-click .ti-close").click(function(){
    $('.search-box-update').removeClass('show-search-udpate')
});


$(".update-sidebar-menu").click(function(){
    $('.update-box-filter').addClass('show-filter-udpate')
});

$(".update-btn-close i").click(function(){
    $('.update-box-filter').removeClass('show-filter-udpate')
});